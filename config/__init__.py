
import falcon
import sentry_sdk
import mongoengine
from spectree import SpecTree
from config import base
import os
from config.openapi import PAGES
from config.openapi import PATH
from config.openapi import TITLE
from config.openapi import VERSION


sentry_sdk.init(
    os.environ.get("SENTRY_DSN"),
    traces_sample_rate=1.0
)

"""
Initialize falcon
"""
app = application = falcon.API(
)
"""
Initialize OpenApi
"""
api = SpecTree(
    'falcon',
    title=TITLE,
    version=VERSION,
    path=PATH,
    page=PAGES["swagger"]
)

"""
Initialize URL
"""
from app.api.v1 import urls

api.register(app)

"""
Initialize MongoDB
"""
mongoengine.connect(
    base.NAME,
    host=base.HOST,
    port=27017,
    username=base.USER,
    password=base.PASSWORD
)


