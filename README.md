# Books Service

# Description

1. Diseñar la base de datos de acuerdo a los atributos del libro.
2. Crear un servicio para buscar un libro por cualquiera de sus atributos. Si no hay
registros en la base de datos interna, el servicio debe hacer la búsqueda en el API
de google y entregar los resultados.
a. En la respuesta del APi se debe indicar la fuente, Ej: db interna, google
3. Registrar un libro. Si la fuente en la respuesta del punto 2 no es la DB interna, se
debe poder crear un libro con el resultado de esa búsqueda.
4. Integrar otra API de consulta de libros.
5. Eliminar un libro.

# Library and Package

-   Language: Python3
-   Framework: Falcon
-   Database: MongoDB
-   Container: Docker
-   OpenApi
-   Docker

#
# Instalation

1.  Install Virtualenv and activate it

```
virtualenv - p python3 venv
```

```
source venv/bin/activate
```

2.  Install requirements.txt

3. pip3 install git+https://github.com/oscfrayle/spectree

```
pip3 install - r requirements.txt
```
-   Libraries:
    - falcon
    - spectree
    - mongoengine
    - gunicorn


**Docs**

```
/api/v1/docs
```
**run Server**
```
$ gunicorn config --bind=0.0.0.0:8001
```


3. Status services
    /health


# Resource

- Create Book 

```
POST  /api/v1/book
```
Body

```
{
    "id": "lwERBwAAQBAJ",
    "title": "secreto del Bambú",
    "subtitle": "Una fAbula",
    "authors": [
        "Ismael Cala"
    ],
    "category": ["Fiction"],
    "publication_date": "2015-09-08",
    "editor": "",
    "description": "Una fabula de inspiración y transformación personal para descubrir nuestra verdadera libertad interio",
    "image": "http://books.google.com/books/content?id=lwERBwAAQBAJ&printsec=frontcover&img=1&zoom=1&edge=curl&source=gbs_api",
    "source": "Api Google"
}

```
response

```
{
    "book_id": "lwERBwAAQBAJ1"
}
```

- GET Book 

```
POST  /api/v1/book
```
params

```
book_id: str
title: str
subtitle: str
authors: list
category: list
publication_date: str
editor: str
description: str
image: str

```

response

```
{
    "data": {
        "id": "RrcoAPSR",
        "title": "El Principito",
        "subtitle": "Principito",
        "author": [
            "principito2"
        ],
        "categories": [
            "Ficcion"
        ],
        "publication_date": "2012-10-04",
        "editor": "ABC",
        "description": "asmdasmds",
        "image": "",
        "source": "DB Local"
    }
}
```
- Delete Book 

```
DELETE  /api/v1/book/{book_id}
```
response
```
{
    "data": {
        "book_id": "lwERBwAAQBAJ"
    }
}
```
