from enum import unique
from mongoengine import StringField
from mongoengine import DateTimeField
from mongoengine import BooleanField
from mongoengine import Document
from mongoengine import ListField
from datetime import datetime


class Base:
    """
    """
    created_at = DateTimeField(default=datetime.now())
    updated_at = DateTimeField()
    deleted_at = DateTimeField()
    active = BooleanField(default=True)
    deleted = BooleanField(default=False)
    trash = BooleanField(default=False)


class Book(Document, Base):
    """
    """
    book_id = StringField(unique=True)
    title = StringField(unique=True)
    subtitle = StringField()
    authors = ListField()
    category = ListField()
    publication_date = StringField()
    editor = StringField()
    description = StringField()
    image = StringField()
    source = StringField()
