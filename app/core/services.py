import logging

# exception
from app.core.exceptions import APIBookNotFound
from app.core.exceptions import GeneraException
# models
from app.core.models import Book
# constants
from app.core.utils.constants import HEADERS_RAPIDAPI
from app.core.utils.constants import HOST_GOOGLE_BOOK
from app.core.utils.constants import HOST_RAPIDAPI
# utils
from app.core.utils.utils import generate_book_id, get_http
from sentry_sdk import capture_exception

# generic queryset
from .query_managers.queryset import GenericQuery


class BookServices:
    """
    Contains the CRUD methods for the Book
    """

    def create(self, data):
        """
        Create Book
        """
        try:
            if not data.get('id'):
                # generate book_id
                id_book = generate_book_id()
                # add book_id
                data['book_id'] = id_book
            else:
                data['book_id'] = data.get('id')
                data.pop('id')

            # create book
            create = GenericQuery(Book).save(data)

            logging.info('Create book')

            return create
        except Exception as e:
            logging.error('error create book')
            capture_exception(e)
            raise APIBookNotFound(e.message, 404)

    def get_book(self, data):
        """
        return one object
        """
        # query
        book = GenericQuery(Book).filter(data)

        # valida si existe el libro en la db local
        if book:
            _book = book.first()
            book = {
                "id": _book.book_id,
                "title": _book.title,
                "subtitle": _book.subtitle,
                "author": _book.authors,
                "categories": _book.category,
                "publication_date": _book.publication_date,
                "editor": _book.editor,
                "description": _book.description,
                "image": _book.image,
                "source": "DB Local"
            }
            return book

        try:
            api_google = ApiBooks.api_google(data)
            self.create(api_google)
            logging.info('http api_rapidapi')
            return api_google
        except Exception:
            api_rapidapi = ApiBooks.api_rapidapi(data)
            self.create(api_rapidapi)
            logging.info('http api_rapidapi')
            return api_rapidapi

    def update_book(self, data):
        """
        Update data book
        """
        update = GenericQuery(Book).update(data)
        return update

    def delete_book(self, data):
        """
        This method does not delete the data, it deactivates it
        marking the field active=false
        """
        GenericQuery(Book).update(data, "active", False)
        logging.info('delete book')

        return


class ApiBooks:
    """
    """
    @ staticmethod
    def api_google(data: dict):
        """
        realiza la busqueda en la api de google book
        :: Parameters
            - id
            - Title
            - Subtitle
        """
        try:
            image = ""
            params = {'q': data.get('title'), 'maxResults': 1}

            get_book = get_http(HOST_GOOGLE_BOOK, params)

            _book = get_book.get('items')[0]['volumeInfo']

            if _book.get('imageLinks'):
                image = _book['imageLinks'].get('thumbnail', "")

            book = {
                "id": get_book['items'][0]['id'],
                "title": _book.get('title', ""),
                "subtitle": _book.get('subtitle', ""),
                "authors": _book['authors'],
                "categories": _book['categories'],
                "publication_date": _book.get('publishedDate', ""),
                "editor": _book.get('editor', ""),
                "description": _book.get('description', ""),
                "image": image,
                "source": "Api Google"
            }

            return book
        except Exception:
            capture_exception(APIBookNotFound(
                "Book not Found Api Google", 404))
            raise APIBookNotFound("Book not Found Api Google", {}, 404)

    @ staticmethod
    def api_rapidapi(data: dict):
        """
        realiza la busqueda en la api Rapidapi book
        :: Parameters
            - id
            - Title
        """

        try:
            image = ""
            params = {'hero': data.get('title')}

            get_book = get_http(HOST_RAPIDAPI, params, HEADERS_RAPIDAPI)

            if get_book.get('images'):
                image = get_book.get('md', "")

            book = {
                "id": get_book.get('id'),
                "title": get_book.get('name', ""),
                "subtitle": get_book.get('subtitle', ""),
                "authors": get_book.get('authors', ""),
                "categories": get_book.get('categories'),
                "publication_date": get_book.get('publishedDate', ""),
                "editor": get_book.get('editor', ""),
                "description": get_book.get('description', ""),
                "image": image,
                "source": "Api Rapidapi"
            }

            return book
        except Exception as e:
            capture_exception(APIBookNotFound(
                "Book not Found Api Rapidapi", 404))
            raise APIBookNotFound("Book not Found Api Rapidapi", {}, 404)
