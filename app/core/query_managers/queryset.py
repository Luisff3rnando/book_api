from sentry_sdk import capture_exception
from mongoengine.errors import NotUniqueError
from mongoengine.errors import DoesNotExist
# interface
from .mixin_queryset import Query
# exception
from app.core.exceptions import GeneraException


class GenericQuery(Query):
    """
    Query managers based in MongoDb
    """

    def __init__(self, entity, message=None):
        self.entity = entity
        self.message = message

    def save(self, data):
        try:
            _intance = self.entity.objects.create(**data)
            return _intance
        except NotUniqueError:
            # capture_exception(NotUniqueError)
            raise GeneraException("book already exists", 422)

    def get(self, data):
        try:
            _intance = self.entity.objects.get(**data)
            return _intance
        except DoesNotExist:
            capture_exception(DoesNotExist)
            raise GeneraException("book not found", {}, 422)

    def filter(self, data):
        try:
            _intance = self.entity.objects.filter(**data)
            return _intance
        except Exception as e:
            capture_exception(e)
            raise GeneraException(e, {}, 422)

    def update(self, filter, key, data):
        try:
            _intance = self.entity.objects(
                **filter).update_one(**{'set__{}'.format(key): data})
            return _intance
        except Exception as e:
            capture_exception(e)
            raise GeneraException(e, {}, 422)

    def delete(self, data):
        try:
            _intance = self.entity.objects.filter(**data).delete()
            return _intance
        except Exception as e:
            capture_exception(e)
            raise GeneraException(e, {}, 422)
