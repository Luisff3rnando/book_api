"""
"""
import json
import requests
import random
import string

from sentry_sdk import capture_exception


def get_http(host: str, query_params: str, headers: dict = None):
    url = host

    if not headers:
        headers = {
            'accept': '*/*',
            'Content-Type': 'application/json'
        }

    try:
        response = requests.get(
            url,
            headers=headers,
            params=query_params
        )
        if response.status_code != 200:
            raise
        return json.loads(response.content)
    except Exception as e:
        capture_exception(e)
        raise Exception(e)


def generate_book_id():
    """
    """
    number_of_strings = 5
    length_of_string = 8
    for x in range(number_of_strings):
        _name = (''.join(random.choice(string.ascii_letters + string.digits)
                 for _ in range(length_of_string)))

        return _name
