"""
"""
import os


HOST_GOOGLE_BOOK = os.environ.get("HOST_BOOK_GOOGLE")
HOST_RAPIDAPI = os.environ.get("HOST_BOOK_RAPIDAPI")

HEADERS_RAPIDAPI = {
    os.environ.get("HEADER_RAPIDAPI_KEY"): os.environ.get("RAPIDAPI_KEY")
}
