import falcon
import pytest
from app.api.v1.urls import app
from falcon import testing


class MyTestCase(testing.TestCase):
    def setUp(self):
        super(MyTestCase, self).setUp()

        # Assume the hypothetical `myapp` package has a
        # function called `create()` to initialize and
        # return a `falcon.App` instance.
        self.app = app


class TestMyApp(MyTestCase):
    def test_create_book(self):

        endpoint = '/api/v1/book'

        body = {
            "id": "lwERBwAAQBAJ1232",
            "title": "Mujer maravilla",
            "subtitle": "Una fAbula2",
            "authors": [
                "Ismael Cala2"
            ],
            "category": ["Fiction"],
            "publication_date": "2015-09-08",
            "editor": "",
            "description": "Una fabula de inspiración y transformación personal para descubrir nuestra verdadera libertad interio",
            "image": "http://books.google.com/books/content?id=lwERBwAAQBAJ&printsec=frontcover&img=1&zoom=1&edge=curl&source=gbs_api"
        }

        response = {
            "book_id": "lwERBwAAQBAJ10"
        }

        result = self.simulate_post(
            endpoint,
            json=body,
            headers={'content-type': 'application/json'}
        )

        print("*******************")
        print(result.json)
        print("*******************")
        assert result.status == falcon.HTTP_CREATED


# def test_get_one_book(client):
#     endpoint = '/api/v1/book?book_id=lwERBwAAQBAJ3'
#     result = client.simulate_get(endpoint)

#     # assert result.json==doc
#     assert result.status == falcon.HTTP_OK
