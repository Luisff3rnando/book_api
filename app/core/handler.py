from app.core.services import BookServices


class Handlerbooks:
    """
    docstring
    """
    @staticmethod
    def create(kwargs):
        _status = BookServices().create(kwargs)
        return _status

    @staticmethod
    def get(kwargs):
        _status = BookServices().get_book(kwargs)
        return _status

    @staticmethod
    def update(kwargs):
        _status = BookServices().update_book(kwargs)
        return _status

    @staticmethod
    def delete(kwargs):
        _status = BookServices().delete_book(kwargs)
        return _status
