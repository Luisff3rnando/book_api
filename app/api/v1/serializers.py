from uuid import UUID

from pydantic import BaseModel


class SuccessSerializer(BaseModel):
    """
    serializer respuesta swagger
    """
    message: str = "OK"


class BookSerializer(BaseModel):
    """
    serializer respuesta swagger
    """
    title: str
    subtitle: str
    authors: list
    category: list
    publication_date: str
    editor: str
    description: str
    image: str


class BookFilterSerializer(BaseModel):
    """
    serializer respuesta swagger
    """
    book_id: str
    title: str = None
    subtitle: str = None
    authors: list = None
    category: list = None
    publication_date: str = None
    editor: str = None
    description: str = None
    image: str = None
