from config import app

from .views import BookView
from .views import DeleteBook
from .views import HealthCheckView

app.add_route(
    '/health',
    HealthCheckView()
)
app.add_route(
    '/api/v1/book',
    BookView()
)
app.add_route(
    '/api/v1/book/{book_id}',
    DeleteBook()
)
