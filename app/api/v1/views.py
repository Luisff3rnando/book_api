import json
import falcon
from spectree import Response
from config import api

from app.core.handler import Handlerbooks
from app.api.v1.serializers import SuccessSerializer
from app.api.v1.serializers import BookSerializer
from app.api.v1.serializers import BookFilterSerializer


class HealthCheckView:
    """
    Health check resource
    """
    @api.validate(
        json=SuccessSerializer,
        resp=Response(
            HTTP_200=None,
            HTTP_403=None
        ), tags=['Status']
    )
    def on_get(self, request, response):
        _response = "Ok"
        response.body = json.dumps(_response)
        response.status = falcon.HTTP_200


class BookView:
    """
    """
    @api.validate(
        json=BookSerializer,
        resp=Response(
            HTTP_200=None,
            HTTP_403=None
        ), tags=['Book']
    )
    def on_post(self, request, response, **kwargs):
        """
        Create Book
        """
        try:
            book = Handlerbooks.create(
                request.media,
            )
            output = {
                'book_id': str(book.book_id)
            }
            response.status = falcon.HTTP_201
            response.media = output
        except (UnicodeDecodeError, Exception) as e:
            response.body = json.dumps(dict(
                error=True,
                description=str(e)
            ))
            response.status = falcon.HTTP_422

    @api.validate(
        query=BookFilterSerializer,
        resp=Response(
            HTTP_200=None,
            HTTP_403=None
        ), tags=['Book']
    )
    def on_get(self, request, response, **kwargs):
        """
        Filter books by params
        Params :
        - book_id: str
        - title: str
        - subtitle: str
        - authors: list
        - category: list
        - publication_date: str
        - editor: str
        - description: str
        - image: str
        """
        try:
            book = Handlerbooks.get(request.params)
            output = {"data": book}
            response.status = falcon.HTTP_200
            response.media = output
        except (UnicodeDecodeError, Exception) as e:
            response.body = json.dumps(dict(
                error=True,
                description=str(e)
            ))
            response.status = falcon.HTTP_422


class DeleteBook:
    @api.validate(
        resp=Response(
            HTTP_200=None,
            HTTP_403=None
        ), tags=['Book']
    )
    def on_delete(self, request, response, **kwargs):
        """
        Delete Book
        """

        try:
            Handlerbooks.delete(kwargs)
            output = {"data": kwargs}
            response.status = falcon.HTTP_200
            response.media = output
        except (UnicodeDecodeError, Exception) as e:
            response.body = json.dumps(dict(
                error=True,
                description=str(e)
            ))
            response.status = falcon.HTTP_422
